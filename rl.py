# https://medium.com/swlh/policy-gradient-reinforcement-learning-with-keras-57ca6ed32555

from keras.layers import Dense
from keras.optimizers import Adam
from keras.models import Sequential
from keras import backend as K
import keras
import gym
import gym_atm
import numpy as np
import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
from gym_atm.envs.config.hw_config import ncr001, ncr002
from gym_atm.envs.config.service_config import service_config_3

import logging
import os
import datetime

FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")

if not os.path.exists("./log"):
    log_path = os.path.join(os.getcwd(), "log")
    os.makedirs(log_path)

def getFileHandler(path, filename):
    file_handler = logging.FileHandler(os.path.join(path, filename+'.log'))
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(FORMATTER)
    return file_handler

def getConsoleHandler():
    handler = logging.StreamHandler()
    handler.setLevel(logging.ERROR)
    handler.setFormatter(FORMATTER)
    return handler




## Config ##
ENV = "atm-env-v0"
RANDOM_SEED = 1
N_EPISODES = 500
MAX_ITERS = 100

# random seed (reproduciblity)
np.random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)

# set the env
env = gym.make('atm-env-v0', atm_config=ncr002, service_config=service_config_3)
env.seed(RANDOM_SEED)
env.reset()  # reset to env


class REINFORCE:
    def __init__(self, env, path=None):
        self.env = env  # import env
        self.state_shape = env.observation_space.n  # the state space
        self.action_shape = env.action_space.n  # the action space
        self.gamma = 0.99  # decay rate of past observations
        self.alpha = 1e-4  # learning rate in the policy gradient
        self.learning_rate = 0.01  # learning rate in deep learning

        if not path:
            self.model = self._create_model()  # build model
        else:
            self.model = self.load_model(path)  # import model

        # record observations
        self.states = []
        self.gradients = []
        self.rewards = []
        self.probs = []
        self.discounted_rewards = []
        self.total_rewards = []

        #logging
        self.logdir = os.path.join(os.getcwd(), 'log', datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
        os.makedirs(self.logdir)
        self.logger = logging.getLogger("aaa")
        #self.logger.setLevel(level=logging.DEBUG)
        self.logger.addHandler(getConsoleHandler())


    def hot_encode_action(self, action):
        '''encoding the actions into a binary list'''

        action_encoded = np.zeros(self.action_shape, np.float32)
        action_encoded[action] = 1

        return action_encoded

    def remember(self, state, action, action_prob, reward):
        '''stores observations'''
        encoded_action = self.hot_encode_action(action)
        self.gradients.append(encoded_action - action_prob)     #Разница между целевой вероятностью и фактической
        self.states.append(state)
        self.rewards.append(reward)
        self.probs.append(action_prob)

    def _create_model(self):
        ''' builds the model using keras'''

        hln = np.ceil(np.sqrt(self.state_shape*self.action_shape))
        print(hln)
        model = Sequential()

        # input shape is of observations
        model.add(Dense(self.state_shape, input_shape=[self.state_shape], activation="relu"))
        # add a relu layer
        model.add(Dense(hln, activation="relu"))

        # output shape is according to the number of action
        # The softmax function outputs a probability distribution over the actions
        model.add(Dense(self.action_shape, activation="softmax"))
        model.compile(loss="categorical_crossentropy",
                      optimizer=Adam(lr=self.learning_rate))

        return model

    def get_action(self, state):
        '''samples the next action based on the policy probabilty distribution
          of the actions'''

        # transform state
        state = state.reshape([1, state.shape[0]])
        # get action probably
        action_probability_distribution = self.model.predict(state).flatten()
        # norm action probability distribution
        action_probability_distribution /= np.sum(action_probability_distribution)

        # sample action
        action = np.random.choice(self.action_shape, 1,
                                  p=action_probability_distribution)[0]

        return action, action_probability_distribution

    def get_discounted_rewards(self, rewards):
        '''Use gamma to calculate the total reward discounting for rewards
        Following - \gamma ^ t * Gt'''

        discounted_rewards = []
        cumulative_total_return = 0
        # iterate the rewards backwards and and calc the total return
        for reward in rewards[::-1]:
            cumulative_total_return = (cumulative_total_return * self.gamma) + reward
            discounted_rewards.insert(0, cumulative_total_return)

        # normalize discounted rewards
        mean_rewards = np.mean(discounted_rewards)
        std_rewards = np.std(discounted_rewards)
        norm_discounted_rewards = (discounted_rewards - mean_rewards) / (std_rewards + 1e-7)  # avoiding zero div

        return norm_discounted_rewards

    def update_policy(self):
        '''Updates the policy network using the NN model.
        This function is used after the MC sampling is done - following
        \delta \theta = \alpha * gradient + log pi'''

        # get X
        states = np.vstack(self.states)

        # get Y
        gradients = np.vstack(self.gradients)   #Трансформируем list of ndarray в 2D-ndarray
        rewards = np.vstack(self.rewards)
        discounted_rewards = self.get_discounted_rewards(rewards)
        gradients *= discounted_rewards
        gradients = self.alpha * np.vstack([gradients]) + self.probs

        history = self.model.train_on_batch(states, gradients)

        self.states, self.probs, self.gradients, self.rewards = [], [], [], []

        return history

    def train(self, episodes, rollout_n=1, render_n=10):
        '''train the model
        episodes - number of training iterations
        rollout_n- number of episodes between policy update
        render_n - number of episodes between env rendering '''

        env = self.env
        total_rewards = np.zeros(episodes)

        for episode in range(episodes):
            self.logger.handlers = []
            self.logger.addHandler(getFileHandler(self.logdir, "episode_"+str(episode)))
            self.logger.debug("start")

            # each episode is a new game env
            state = env.reset()
            done = False
            episode_reward = 0  # record episode reward
            iter = 0



            while not done:
                # play an action and record the game state & reward per episode
                action, prob = self.get_action(state)
                next_state, reward, done, _ = env.step(action)
                self.remember(state, action, prob, reward)

                message = "State: {0} | action: {1} | new_state: {2} | reward: {3}".format(state, action, next_state, reward)
                self.logger.debug(message)

                state = next_state
                episode_reward += reward
                iter += 1

                if episode % render_n == 0:  ## render env to visualize.
                    #message = env.render()
                    #print("Episode: {0} | {1}".format(episode, message))
                    pass
                if done:
                    # update policy
                    if episode % rollout_n == 0:
                        history = self.update_policy()

            total_rewards[episode] = episode_reward
            message = env.render()
            print("Episode: {0} | {1}".format(episode, message))

        self.total_rewards = total_rewards
        return self.model

if __name__ == "__main__":
    print("run")
    r = REINFORCE(env)
    model = r.train(750)
    model.save('4codes_750-iters')