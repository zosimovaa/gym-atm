import logging
from .atm import Atm2
from .availability import Availability
from gym import spaces
import gym


class AtmEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    TRANSACTION_RATE = 20               # Количество транзакций за раз
    TRANSACTION_SCALE_FACTOR = 0.1      # Понижающий коэффициент для награды за транзакции
    ACTION_SUCCESS_SCALE_FACTOR = 0.5   # Количество штрафных балов, начисляемое при успешном ремонте
    TRANSACTION_PER_HOUR_RATE = 10

    EPISODE_MODE = 1                    # 1: по транзакциям,  0:  фонд времени
    EPISODE_LIMIT = 1000                # лимит для окончания эпизода

    def __init__(self, atm_config=None, service_config=None, debug=False,log=False):
        super(AtmEnv, self).__init__()

        self.atm_config = atm_config
        self.service_config = service_config
        self.atm = Atm2(self.atm_config, self.service_config)

        self.eval = Availability(scale_factor=self.TRANSACTION_PER_HOUR_RATE)

        n_action_space = len(self.service_config)
        n_observation_space = len(self.atm.get_state())

        self.action_space = spaces.Discrete(n_action_space)
        self.observation_space = spaces.Discrete(n_observation_space)

        self.logger = logging.getLogger('ATM_ENV')
        if debug:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)
        self.logger.debug("Initialized")

        return

    # todo разобраться с типом action
    def step(self, action):
        """

        :param action: - дискетный тип. 0, 1, 2, 3 и так далее. Необходимо трансформировать в название действия.
        :return:
        """

        # 0.  Сконвертировать экшн от RL алгоритма в запись из сервисного конфига
        atm_action = list(self.service_config.keys())[action]

        # 1. Применяем действие. Получаем результат (успех/неуспех), длительность, штраф
        status_code, processed_volume, success, wo_time, penalty = self.atm.apply_action(atm_action)
        self.eval.reg(processed_volume, wo_time, penalty)

        # 2. Считаем награду. На вход подаем данные, полученные на шагах 1 и 2
        reward = self._reward(status_code, success, wo_time, penalty, processed_volume)

        # 3. Запрашиваем статус банкомата (закодированный), возвращаем как observation.
        observation = self.atm.get_state()

        # 4 Оцениваем завершение эпизода
        done = False
        if self.EPISODE_MODE:
            if self.eval.transactions >= self.EPISODE_LIMIT:
                done = True
        else:
            if self.eval.total_time >= self.EPISODE_LIMIT:
                done = True

        # 5. Прочие параметры

        info = {}

        return observation, reward, done, info

    def reset(self):
        # Создаем новый инстансы
        self.atm = Atm2(self.atm_config, self.service_config)
        self.eval.reset()

        # Пробуем совершить операцию.
        self.atm.transaction(volume=self.TRANSACTION_RATE)

        observation = self.atm.get_state()
        return observation

    def render(self, mode='human', close=False):
        if self.EPISODE_MODE:
            message = "Потребовалось времени: {0}, длительность заявок: {1}, штраф: {2}".format(self.eval.total_time, self.eval.wo_time, self.eval.penalty)
        else:
            message = "Длительность заявок: {0}, штраф: {1}".format(self.eval.wo_time, self.eval.penalty)
        return message

    def _reward(self, status_code, success, wo_time, penalty, processed_volume) -> float:
        """
        Метод рассчитывает награду
        Политика:
        1. За совершенные операции начисляется положительная награда. Небольшая. Размер определяет TRANSACTION_SCALE_FACTOR
        2. За исполнение заявки начисляется штраф в виде времени простоя
        3. Если заявка успешна - режем штраф пополам (эта тактика под вопросом, попробовать не резать)
        4. К штрафу плюсуем penalty от партнера

        :param status_code: Статус код на момент расчета награды. [Пока не используется].
        :param success: Успешность выполнения заявки
        :param wo_time: Длительность заявки
        :param penalty: Штраф за заявку
        :param processed_volume: Совершенное количество операций
        :return: значение наград
        """
        transaction_reward = self.TRANSACTION_SCALE_FACTOR * ((processed_volume ** 2)/self.TRANSACTION_RATE)
        action_reward = -1 * success * self.ACTION_SUCCESS_SCALE_FACTOR * wo_time - penalty
        reward = transaction_reward + action_reward
        return reward