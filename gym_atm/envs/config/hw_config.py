ch = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 200,           # Ресурс
            "available_resource": 140,
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        1100: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 160,
            "FLM": [1.0, 0.1],
            "SLM": [1.0, 1.0]
        },
        1200: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 180,
            "FLM": [0.2, 1.0],
            "SLM": [1.0, 1.0]
        },
        1300: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 200,
            "FLM": [0.0, 0.0],
            "SLM": [1.0, 1.0]
        }
    }
}

cr = {
    "name": "Card Reader",
    "alias": "CR",
    "errors": {
        2000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 100,           # Ресурс
            "FLM": [0.0, 0.5],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        2200: {
            "err_level": 2,
            "resource": 1111,
            "FLM": [0.5, 1.0],
            "SLM": [1.0, 1.0]
        }
    }
}

down = {
    "name": "DOWN",
    "alias": "DOWN",
    "errors": {
        3000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 155,           # Ресурс
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [0.0, 0.0]
        },
        3200: {
            "err_level": 2,
            "resource": 342,
            "FLM": [0.0, 0.0],
            "SLM": [1.0, 1.0]
        }
    }
}

#=========================================

ncr001 = {
    "ID": 389000,
    "devices": [ch]
}




ch1 = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 200,           # Ресурс
            "available_resource": 190,
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        }
    }
}

cr1 = {
    "name": "Card Reader",
    "alias": "CR",
    "errors": {
        2000: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 160,
            "FLM": [1.0, 0.1],
            "SLM": [1.0, 1.0]
        }
    }
}

epp1 = {
    "name": "EPP",
    "alias": "EPP",
    "errors": {
        3000: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 130,
            "FLM": [0.1, 1.0],
            "SLM": [1.0, 1.0]
        }
    }
}

bna1 = {
    "name": "Bank Note Acceptor",
    "alias": "BNA",
    "errors": {
        4000: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 100,
            "FLM": [0.0, 0.0],
            "SLM": [1.0, 1.0]
        }
    }
}

ncr002 = {
    "ID": 389000,
    "devices": [ch1, cr1, epp1, bna1]
}