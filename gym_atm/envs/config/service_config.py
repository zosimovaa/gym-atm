service_config_basic = {
    "FLM": 3,
    "SLM": 8,
    "INK": 12
}

service_config_2 = {
    "ZRM": 3,
    "FLM": 4,
    "SLM": 12,
    "INK": 18,
    "SLM_DOWN": 20,
    "OK": 0
}

service_config_3 = {
    "FLM": {"response_time": 4, "false_claim": 2, "task_type": 2},
    "SLM": {"response_time": 12, "false_claim": 4, "task_type": 3},
    "TRANSACTION": {"response_time": 0, "false_claim": 0, "task_type": 0},
}
service_config_31 = {
    "ZRM": {"response_time": 4, "false_claim": 2, "task_type": 1},
    "FLM": {"response_time": 4, "false_claim": 2, "task_type": 2},
    "SLM": {"response_time": 12, "false_claim": 2, "task_type": 3},
    "INK": {"response_time": 18, "false_claim": 2, "task_type": 4},
    "SLM_DOWN": {"response_time": 24, "false_claim": 2, "task_type": 5},
    "TRANSACTION": {"response_time": 1, "false_claim": 0, "task_type": 0},
}

service_config_4 = [
    {"name": "TRANSACTION", "response_time": 1, "false_claim": 0, "task_type": 0},
    {"name": "ZRM", "response_time": 4, "false_claim": 2, "task_type": 1},
    {"name": "FLM", "response_time": 4, "false_claim": 2, "task_type": 2},
    {"name": "SLM", "response_time": 12, "false_claim": 2, "task_type": 3},
    {"name": "INK", "response_time": 18, "false_claim": 2, "task_type": 4},
    {"name": "SLM_DOWN", "response_time": 24, "false_claim": 2, "task_type": 5},
]

"""
Нейронка выдает какоц-то прогноз
прогноз бинарный

может быть все нули? в принципе - да. Будем расченивать это как транзакция

Если такой вариант, то экшены не должны включать в себя TRANSACTION и его надо хардкодить.


НС -> [0,1,0,0,0]
Выбор действия удобно делать из массива





"""