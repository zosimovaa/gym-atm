import numpy as np
import logging
import seaborn as sns
import matplotlib.pyplot as plt
class Availability:
    PERIOD_LIMIT = 168    #количество часов в неделю

    def __init__(self, scale_factor=1):
        self.scale_factor = scale_factor  # количество транзакций за 1 час

        # Поля под расчет доступности (пока не используется)
        self.all_time = []
        self.all_fault = []
        self.curr_time = 0
        self.curr_fault = 0

        # Поля для расчета результатов эпизода
        self.total_time = 0
        self.transactions = 0
        self.penalty = 0
        self.wo_time = 0

        self.logger = logging.getLogger('ATM')
        self.logger.debug("Initialized")

    def reg_time(self, fault_time=0, transaction=0):
        """
        Метод фиксирует количество совершенных операций и переводит их в фонд времени
        Либо фиксирует время простоя (уже в единицах времени).
        Учтенные данные делятся на временные периоды. Длина такого периода определяется параметром PERIOD_LIMIT
        :param fault_time: время простоя
        :param transaction: количество совершенных транзакций
        :return: фонд времени
        """
        tm = fault_time if transaction == 0 else transaction / self.scale_factor
        tm_to_return = tm
        while tm:
            if (self.curr_time + tm) > self.PERIOD_LIMIT:
                #определили дельту, которая влезет в текущий период
                delta = self.PERIOD_LIMIT - self.curr_time

                #определить значения для переноса в следующий круг (tm и fault_time)
                if fault_time:
                    fault_time = max(0, tm - delta)
                tm = tm - delta

                #Заполнить текущие значения
                self.all_time.append(self.PERIOD_LIMIT)
                if fault_time:
                    self.all_fault.append(self.curr_fault + delta)
                else:
                    self.all_fault.append(self.curr_fault)

                #Обнулить значения текущего периода
                self.curr_time, self.curr_fault = 0, 0

            else:
                self.curr_time += tm
                self.curr_fault += fault_time
                tm = 0
        return tm_to_return

    def reg(self, processed_volume, wo_time, penalty):
        work_time = processed_volume / self.scale_factor

        self.total_time += work_time
        self.total_time += wo_time
        self.wo_time += wo_time
        self.transactions += processed_volume
        self.penalty += penalty
        return

    def reset(self):
        """
        Метод сбрасывает состояние и совершенный ранее учет
        :return:
        """
        self.all_time = []
        self.all_fault = []
        self.curr_time = 0
        self.curr_fault = 0

        self.total_time = 0
        self.transactions = 0
        self.penalty = 0
        self.wo_time = 0
        return

    def status(self):
        """
        Метод визуализирует текущее состояние по простою в виде доступности
        :return:
        """
        return self.transactions, self.total_time, self.wo_time, self.penalty
