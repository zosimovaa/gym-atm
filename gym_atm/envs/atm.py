import logging
import numpy as np
from .device import Device2, AtmDeviceError
from .availability import Availability

logging.basicConfig(level=logging.ERROR, format='%(asctime)s %(name)s [%(levelname)s]: %(message)s')


class Atm2:
    """
    Класс эмулирует модель банкомата
    """

    BAD_ACTION = {"response_time": 0, "false_claim": 2, "task_type": 1}
    SCALE_FACTOR = 1

    TRANSACTION_RATE = 5

    def __init__(self, atm_config, service_config):
        """
        Инициализация экземпляра банкомата
        :param atm_config: конфиг устройств
        :param service_config: конфиг сервисного обслуживания
        """
        self.status = {
            "code": 0,
            "desc": None,
            "error": None
        }
        self.service = service_config
        self.atm_config = atm_config
        self.devices = {}
        for dev_cfg in self.atm_config["devices"]:
            self.devices[dev_cfg["alias"]] = Device2(dev_cfg)

        self.down_time = 0
        self.logger = logging.getLogger('ATM')
        self.logger.debug("Initialized")
        return

    def transaction(self, volume=0):
        """
        Метод эмулирует проведение транзакций и "списывает" реасурс по узлам.
        :param volume: Количество совершенных транзакций. При значении 0 совершается максимальное количество операций и
        приводит к переходу узла в ошибку.
        :param encoded: тип возвращаемого статуса - код ошибки или one-hot encoded вектор
        :return: объем транзакций
        """
        processed_volume = 0
        if self.status["code"] == 0:
            resource = min([self.devices[key].get_available_resource() for key, value in self.devices.items()])
            volume = resource if volume == 0 else min(volume, resource)
            for key, value in self.devices.items():
                status, processed_volume = self.devices[key].utilize_resource(volume)
                if (status != 0) and (self.status["code"] == 0):
                    self.status["code"] = status
                    self.status["desc"] = value.alias

        self.logger.info("ATM. Processed volume: {0}. | Volume: {1}".format(processed_volume, self.status["code"]))
        return processed_volume

    def apply_action(self, action, volume=TRANSACTION_RATE):
        """
        Метод эмулирует исполнение заявки (FLM, SLM и т.д.). По итогу ошибка будет
        сброшена с восстановлением некоторого процента ресурса или действие будет безуспешным (ошибка сохранится).
        :param action: тип применяемого действия
        :param encoded: тип возвращаемого статуса - код ошибки или one-hot encoded вектор
        :return: Статус банкомата (код ошибки) и "награда".
        """

        processed_volume = 0
        success = True
        down_time = 0
        penalty = 0
        restored_resource = 0

        if self.status["code"] != 0:
            action_details = self.service.get(action, self.BAD_ACTION)
            down_time = action_details["response_time"]
            action_success, restored_resource, is_fail_claim = self.devices[self.status["desc"]].apply_action(action)

            if is_fail_claim:
                # Ложный выезд. Статус не изменился
                success = False
                penalty = action_details["false_claim"]
            elif action_success==False:
                # Не удалось устранить ошибку. Статус не изменился
                success = False
            else:
                # Успешное исполнение. Статус изменился. Надо еще раз собрать статусы с узлов.
                self.status["code"] = 0
                self.status["desc"] = None
                for key, value in self.devices.items():
                    dev_state = value.get_state()
                    if dev_state != 0:
                        self.status["code"] = dev_state
                        self.status["desc"] = value.alias
                        break

            self.down_time -= down_time


        else:
            if action == "TRANSACTION":
                processed_volume = self.transaction(volume)
            else:
                action_details = self.service.get(action, self.BAD_ACTION)
                penalty = action_details["false_claim"]
                down_time = action_details["response_time"]

        self.logger.info("ATM | Action result: {0} | Processed volume: {4} | Restored: {1}| Down time: {2} | Penalty: {3}".format(success, restored_resource, self.status["code"], penalty, processed_volume))
        return self.status["code"], processed_volume, success, down_time, penalty

    def reset(self):
        """
        Метод сбрасывает состояние банкомата в исходное
        :return:
        """
        self.devices = {}
        self.down_time = 0

        for dev_cfg in self.atm_config["devices"]:
            self.devices[dev_cfg["alias"]] = Device2(dev_cfg)
        self.logger.debug("ATM reset")
        return

    def get_state(self):
        """
        Метод собирает все возможные статусы ошибок, возвращает в закодированном виде текущий статус.
        :return:
        """
        codes = []
        for key, value in self.devices.items():
            codes += self.devices[key].get_codes()
        state_encoded = np.array(codes) == self.status["code"]
        return state_encoded
