import logging
import numpy as np
import copy

logging.basicConfig(level=logging.ERROR, format='%(asctime)s %(name)s [%(levelname)s]: %(message)s')


class AtmDeviceError(Exception):
    """
    Базовый класс ошибки устроства банкомата
    """
    def __init__(self, code, alias, err, volume):
        self.code = code
        self.alias = alias
        self.err = err
        self.volume = volume


class Device:
    """
    Класс представляет модель узла банкомата.
    Параметры определяются конфигом.
    """
    def __init__(self, config, debug=False):
        self.name = config["name"]
        self.alias = config["alias"]
        self.error_codes = copy.deepcopy(config["errors"])   #todo убрать deepcopy()
        self.state = 0
        self.mileage = 0

        #for key, value in self.error_codes.items():
        #    self.error_codes[key]["available_resource"] = value["resource"]  # * np.random.uniform(0, 1)

        self.logger = logging.getLogger('dev_{}'.format(self.alias))
        self.logger.debug("Device initialized")

    def get_available_resource(self, return_resource=True):
        """
        Метод возвращает доступный ресурс (количество транзакций) до возникновения ошибки.

        :param return_resource: определяет вид, в котором будет возвращен результат - количество транзакций
        до совершения ошибки (True) или массив с данными по каждой ошибке.

        :return: остаток транзакций до ближайшей ошибки или массив с остатками по всем ошибкам
        """
        if self.state != 0:
            available_resource = 0
        else:
            resources = [value["available_resource"] for key, value in self.error_codes.items()]
            available_resource_array = np.array([(r - self.mileage % r) for r in resources])
            available_resource = min(available_resource_array) if return_resource else available_resource_array

        self.logger.debug("Available resource: {0}".format(available_resource))
        return available_resource

    def utilize_resource(self, volume=0):
        """
        Метод утилизирует ресурс узла банкомата.
        :param volume: определяет количество совершенных операций. При значении 0 (по умолчанию) утилизщируется весь
        доступный ресурс
        :return: Статус узла по итогу и утилизированный ресурс
        """
        if (self.state == 0) and (volume >= 0):
            available_resource = self.get_available_resource()
            volume = available_resource if volume == 0 else min(volume, available_resource)
            self.mileage += volume

            for key, value in self.error_codes.items():
                if self.mileage % self.error_codes[key]["available_resource"] == 0:
                    self.state = key
                    break
            if self.state:
                raise AtmDeviceError(self.state, self.alias, self.error_codes[self.state], volume)
        else:
            volume = 0

        self.logger.debug("Processed volume: {0} | State: {1}".format(volume, self.state))
        return self.state, volume

    def apply_action(self, action):
        """
         Метод эмулирует исполнение заявки для восстановления работоспособности узла
        :param action: тип применяемого действия
        :return: Статус, восстановленынй ресурс
        """
        restored_resource = 0
        is_fail_claim = False
        #TODO  Заменить проверку зафиксированного статуса на остаток ресурса.
        if (self.state != 0) and (action in self.error_codes[self.state]):      #есть ошибка и действие применимо к текущей ошибке
            prob, resource_percentage = self.error_codes[self.state][action]
            if prob > np.random.uniform(0, 1):
                # успешное применение действия
                restored_resource = self.__repair(resource_percentage)
                self.logger.debug(
                    "{0}: {1} successful applied. Restored {2:.2%} of resource".format(self.state, action,
                                                                                       resource_percentage))
            else:
                # не прокатило
                self.logger.debug("{0}: {1} fault".format(self.state, action))


        else:
            #ложный выезд (ошибки нет, даже нефатальной)
            is_fail_claim = True

        self.logger.debug("Restored resource: {0} | State: {1}".format(restored_resource, self.state))
        return self.state, restored_resource, is_fail_claim

    def apply_action2(self, action):
        restored_resource = 0
        is_fail_claim = False

        for key, value in self.error_codes.items():
            available_resource = value["available_resource"] - self.mileage % value["available_resource"]





    def __repair(self, resource_percentage):
        """
        Метод восстанавливает ресурс узла на заданный процент от нормального ресурса.
        :param resource_percentage: процент восстановления ресурса
        :return: восстановленный ресурс
        """
        restored_resource = 0
        if resource_percentage > 0:
            restored_resource = self.error_codes[self.state]["resource"] * resource_percentage
            self.error_codes[self.state]["available_resource"] = restored_resource
            self.state = 0
        return restored_resource

    def get_codes(self):
        """
        Метод возвращает список с возможными кодами состояний по устрйоству.
        :return: список кодов ошибок
        """
        codes = list(self.error_codes.keys())
        self.logger.debug("Available error codes: {0}".format(codes))
        return codes



class Device2:
    """
    Класс представляет модель узла банкомата.
    Параметры определяются конфигом.
    """
    def __init__(self, config, debug=False):
        self.name = config["name"]
        self.alias = config["alias"]
        self.error_codes = config["errors"]
        self.state = 0
        self.mileage = 0

    def get_available_resource(self):
        """
        Метод возвращает доступный ресурс (количество транзакций) до возникновения ошибки
        :return: остаток транзакций до ближайшей ошибки
        """
        resources = np.array([value["available_resource"] for key, value in self.error_codes.items()])
        return min(resources)

    def get_state(self):
        self.state = 0
        for key, value in self.error_codes.items():
            if value["available_resource"] == 0:
                self.state = key
                break
        return self.state

    def utilize_resource(self, volume=0):
        """
        Метод утилизирует ресурс узла банкомата.
        :param volume: определяет количество совершенных операций. При значении 0 (по умолчанию) утилизируется весь
        доступный ресурс
        :return: Статус узла по итогу и утилизированный ресурс
        """
        if (self.state == 0) and (volume >= 0):
            available_resource = self.get_available_resource()
            processed_volume = available_resource if volume == 0 else min(volume, available_resource)
            self.mileage += volume
            for key, value in self.error_codes.items():
                self.error_codes[key]["available_resource"] -= processed_volume
                if (self.error_codes[key]["available_resource"] == 0) and (self.state == 0):        # Фиксируем только первый сбой
                    self.state = key
            #if self.state:
            #    raise AtmDeviceError(self.state, self.alias, self.error_codes[self.state], volume_to_process)
        else:
            processed_volume = 0

        return self.state, processed_volume

    def apply_action(self, action):
        """
         Метод эмулирует исполнение заявки для восстановления работоспособности узла
        :param action: тип применяемого действия
        :return: Статус, восстановленынй ресурс
        """
        restored_resource = 0
        is_fail_claim = False
        success = False
        if (self.state != 0) and (action in self.error_codes[self.state]):      #есть ошибка и действие применимо к текущей ошибке
            prob, resource_percentage = self.error_codes[self.state][action]
            if prob > np.random.uniform(0, 1):
                # успешное применение действия
                success = True
                restored_resource = self.error_codes[self.state]["resource"] * resource_percentage
                self.error_codes[self.state]["available_resource"] = restored_resource
            else:
                # неуспешное применение действия
                pass
        else:
            #ложный выезд (ошибки нет, даже нефатальной)
            is_fail_claim = True
        self.get_state()
        return success, restored_resource, is_fail_claim


    def get_codes(self):
        """
        Метод возвращает список с возможными кодами состояний по устрйоству.
        :return: список кодов ошибок
        """
        codes = list(self.error_codes.keys())
        return codes



"""
get_available_resource -вощвращает в банкомат доступный ресурс, чтобы тот инициировал списание.





"""