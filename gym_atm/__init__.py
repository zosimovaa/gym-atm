from gym.envs.registration import register

register(
    id='atm-env-v0',
    entry_point='gym_atm.envs:AtmEnv'
)