from gym_atm.envs.atm import Atm2
from gym_atm.envs.config.service_config import service_config_3
import unittest

ch = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 10,           # Ресурс
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        1100: {
            "err_level": 2,
            "resource": 14,
            "FLM": [1.0, 0.5],
            "SLM": [1.0, 1.0]
        },
        1200: {
            "err_level": 2,
            "resource": 18,
            "FLM": [0.1, 0.1],
            "SLM": [1.0, 1.0]
        }
    }
}

cr = {
    "name": "Card Reader",
    "alias": "CR",
    "errors": {
        2000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 1000,           # Ресурс
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        2100: {
            "err_level": 2,
            "resource": 1400,
            "FLM": [1.0, 0.5],
            "SLM": [1.0, 1.0]
        }
    }
}

ncr001 = {
    "ID": 389000,
    "devices": [ch, cr]
}




class TestAtm(unittest.TestCase):
    #TODO написать тест для метода reward
    #TODO написать тест для метода reset

    def __init__(self, *args, **kwargs):
        super(TestAtm, self).__init__(*args, **kwargs)

    def test_transaction(self):
        atm = Atm2(ncr001, service_config_3)
        # 1. Начальное состояние, ошибки нет
        #self.assertEqual(atm.transaction(volume=5), (0, 5))
        self.assertEqual(atm.apply_action("TRANSACTION", volume=5), (0, 5, True, 0, 0))

        # 2. Добиваем до ошибки
        #self.assertEqual(atm.transaction(), (1000, 5))
        self.assertEqual(atm.apply_action("TRANSACTION", volume=5), (1000, 5, True, 0, 0))
        atm.apply_action("FLM")   #устраняем

        # 3. Вызываем большое число и падаем на следующей ошибке
        self.assertEqual(atm.apply_action("TRANSACTION", volume=2000), (1100, 4, True, 0, 0))
        self.assertEqual(atm.apply_action("TRANSACTION", volume=5), (1100, 0, False, 1, 0))



    def test_apply_action(self):
        atm = Atm2(ncr001, service_config_3)
        # 1. Реакция на действия при состоянии ОК
        self.assertEqual(atm.apply_action("SLM"), (0, 0, True, 0, 2))
        self.assertEqual(atm.apply_action("TRANSACTION"), (0, 5, True, 0, 0))

        # 2. Реакция на действия при ошибке
        self.assertEqual(atm.apply_action("TRANSACTION", volume=10), (1000, 5, True, 0, 0))
        self.assertEqual(atm.apply_action("TRANSACTION"), (1000, 0, False, 1, 0))
        self.assertEqual(atm.apply_action("FLM"), (0, 0, True, 4, 0))



    def test_get_state(self):
        atm = Atm2(ncr001, service_config_3)
        self.assertEqual(len(atm.get_state()), 5)
        atm.transaction()

        self.assertEqual(atm.get_state()[0], True)
        self.assertEqual(atm.get_state()[1], False)
        self.assertEqual(atm.get_state()[2], False)
        self.assertEqual(atm.apply_action("FLM"), (0, 0, True, 4, 0))
        atm.transaction()

        self.assertEqual(atm.get_state()[0], False)
        self.assertEqual(atm.get_state()[1], True)
        self.assertEqual(atm.get_state()[2], False)


if __name__ == "__main__":
    unittest.main()



