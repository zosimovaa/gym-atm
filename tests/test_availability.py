from gym_atm.envs.availability import Availability
import unittest

class TestAvailability(unittest.TestCase):
    def test_reg_time(self):
        a = Availability()
        a.PERIOD_LIMIT = 100
        a.reg_time(transaction=90)
        self.assertEqual(a.curr_time, 90)
        self.assertEqual(a.curr_fault, 0)

        a.reg_time(fault_time=5)
        self.assertEqual(a.curr_time, 95)
        self.assertEqual(a.curr_fault, 5)

        a.reg_time(transaction=10)
        self.assertEqual(a.curr_time, 5)
        self.assertEqual(a.curr_fault, 0)

        a.reg_time(fault_time=99)
        self.assertEqual(a.curr_time, 4)
        self.assertEqual(a.curr_fault, 4)

        self.assertEqual(a.all_time, [100, 100])
        self.assertEqual(a.all_fault, [5, 95.0])

        a.reg_time(transaction=225)
        self.assertEqual(a.curr_time, 29)
        self.assertEqual(a.curr_fault, 0)

        self.assertEqual(a.all_time, [100, 100, 100, 100])
        self.assertEqual(a.all_fault, [5, 95.0, 4.0, 0])


    def test_reg(self):
        a = Availability(scale_factor=100)

        # 1. Фиксируем успешные транзакции
        a.reg(100, 0, 0)
        self.assertEqual(a.total_time, 1)
        self.assertEqual(a.transactions, 100)
        self.assertEqual(a.wo_time, 0)
        self.assertEqual(a.penalty, 0)

        # 2. Фиксируем время заявки
        a.reg(0, 4, 0)
        self.assertEqual(a.total_time, 5)
        self.assertEqual(a.transactions, 100)
        self.assertEqual(a.wo_time, 4)
        self.assertEqual(a.penalty, 0)

        # 3. Фиксируем штраф
        a.reg(0, 0, 2)
        self.assertEqual(a.total_time, 5)
        self.assertEqual(a.transactions, 100)
        self.assertEqual(a.wo_time, 4)
        self.assertEqual(a.penalty, 2)

        # 4. Транзакции и время заявки
        a.reg(300, 12, 0)
        self.assertEqual(a.total_time, 20)
        self.assertEqual(a.transactions, 400)
        self.assertEqual(a.wo_time, 16)
        self.assertEqual(a.penalty, 2)

    def test_reset_after_reg(self):
        a = Availability(scale_factor=100)
        # 1.Заполнение значениями
        a.reg(100, 10, 5)
        self.assertEqual(a.total_time, 11)
        self.assertEqual(a.transactions, 100)
        self.assertEqual(a.wo_time, 10)
        self.assertEqual(a.penalty, 5)

        # 4. Сброс значений
        a.reset()
        self.assertEqual(a.total_time, 0)
        self.assertEqual(a.transactions, 0)
        self.assertEqual(a.wo_time, 0)
        self.assertEqual(a.penalty, 0)

if __name__ == "__main__":
    unittest.main()