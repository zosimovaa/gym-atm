import unittest
from gym_atm.envs.atm_env import AtmEnv
#from gym_atm.envs.config.service_config import service_config_3
#from gym_atm.envs.config.hw_config import ncr001


ch = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 10,           # Ресурс
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        1100: {
            "err_level": 2,
            "resource": 22,
            "FLM": [1.0, 0.5],
            "SLM": [1.0, 1.0]
        },
        1200: {
            "err_level": 2,
            "resource": 25,
            "FLM": [0.1, 0.1],
            "SLM": [1.0, 1.0]
        }
    }
}

cr = {
    "name": "Card Reader",
    "alias": "CR",
    "errors": {
        2000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 1000,           # Ресурс
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        2100: {
            "err_level": 2,
            "resource": 1400,
            "FLM": [1.0, 0.5],
            "SLM": [1.0, 1.0]
        }
    }
}

ncr001 = {
    "ID": 389000,
    "devices": [ch, cr]
}

service_config_3 = {
    "ZRM": {"response_time": 4, "false_claim": 2, "task_type": 1},
    "FLM": {"response_time": 4, "false_claim": 2, "task_type": 2},
    "SLM": {"response_time": 12, "false_claim": 2, "task_type": 3},
    "INK": {"response_time": 18, "false_claim": 2, "task_type": 4},
    "SLM_DOWN": {"response_time": 24, "false_claim": 2, "task_type": 5},
    "OK": {"response_time": 1, "false_claim": 0, "task_type": 0},
}

class EnvAtmTestCase(unittest.TestCase):

    def test_reset(self):
        env = AtmEnv(ncr001, service_config_3)
        env.TRANSACTION_RATE = 5
        observation = env.reset()
        self.assertEqual(len(observation), 5)
        self.assertEqual(observation[0], [False])
        self.assertEqual(observation[1], [False])
        self.assertEqual(observation[2], [False])
        self.assertEqual(observation[3], [False])
        self.assertEqual(observation[4], [False])

    def test_step(self):
        env = AtmEnv(ncr001, service_config_3)
        env.TRANSACTION_RATE = 5

        # 1. Перый шаг, успех
        observation, reward, done, info = env.step("TRANSACTION")
        self.assertEqual(len(observation), 5)
        self.assertEqual(observation[0], False)
        self.assertEqual(observation[1], False)
        self.assertEqual(observation[2], False)
        self.assertEqual(observation[3], False)
        self.assertEqual(observation[4], False)
        self.assertEqual(reward, 0.5)

        # 2. Транзакции проведены, получили ошибку
        observation, reward, done, info = env.step("TRANSACTION")
        self.assertEqual(observation[0], True)
        self.assertEqual(observation[1], False)
        self.assertEqual(observation[2], False)
        self.assertEqual(observation[3], False)
        self.assertEqual(observation[4], False)
        self.assertEqual(reward, 0.5)

        # 3. Применили неуместное действие (ложный выезд)
        observation, reward, done, info = env.step("ZRM")
        self.assertEqual(observation[0], True)
        self.assertEqual(reward, -2)

        # 4. Восстановили ресурс с помощью FLN
        observation, reward, done, info = env.step("FLM")
        self.assertEqual(observation[0], False)
        self.assertEqual(reward, -2)  # todo проверить расчет награды

        # 5. Успешный шаг (как шаг 1). Снова уперлись в ошибку 1
        observation, reward, done, info = env.step("TRANSACTION")
        observation, reward, done, info = env.step("TRANSACTION")
        self.assertEqual(len(observation), 5)
        self.assertEqual(observation[0], True)
        self.assertEqual(observation[1], False)
        self.assertEqual(observation[2], False)
        self.assertEqual(observation[3], False)
        self.assertEqual(observation[4], False)
        self.assertEqual(reward, 0.5)

        # 6. Частичная утилизация транзакций
        observation, reward, done, info = env.step("FLM")
        self.assertEqual(len(observation), 5)
        self.assertEqual(observation[0], False)
        self.assertEqual(observation[1], False)
        self.assertEqual(observation[2], False)
        self.assertEqual(observation[3], False)
        self.assertEqual(observation[4], False)
        self.assertEqual(reward, -2)

if __name__ == '__main__':
    unittest.main()

