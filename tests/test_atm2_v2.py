from gym_atm.envs.atm import Atm2
from gym_atm.envs.config.service_config import service_config_3
import unittest

ch = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 200,           # Ресурс
            "available_resource": 140,
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        1100: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 160,
            "FLM": [1.0, 0.1],
            "SLM": [1.0, 1.0]
        },
        1200: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 185,
            "FLM": [0.2, 1.0],
            "SLM": [1.0, 1.0]
        },
        1300: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 201,
            "FLM": [0.0, 0.0],
            "SLM": [1.0, 1.0]
        }
    }
}

cr = {
    "name": "Card Reader",
    "alias": "CR",
    "errors": {
        2000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 1000,           # Ресурс
            "available_resource": 1000,
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        2100: {
            "err_level": 2,
            "resource": 1400,
            "available_resource": 1400,
            "FLM": [1.0, 0.5],
            "SLM": [1.0, 1.0]
        }
    }
}

ncr001 = {
    "ID": 389000,
    "devices": [ch, cr]
}


class MyTestCase(unittest.TestCase):
    def test_transaction(self):
        atm = Atm2(ncr001, service_config_3)

        # 1. Проводим обычную транзакцию, без ошибки
        processed_volume = atm.transaction(volume=20)
        self.assertEqual(processed_volume, 20)
        self.assertEqual(atm.status["code"], 0)

        # 2. Транзачим до ошибки
        processed_volume = atm.transaction(volume=119)
        self.assertEqual(processed_volume, 119)
        self.assertEqual(atm.status["code"], 0)

        processed_volume = atm.transaction(volume=20)
        self.assertEqual(processed_volume, 1)
        self.assertEqual(atm.status["code"], 1000)

        # 3. Транзачим до следующей ошибки
        state, processed_volume, success, down_time, penalty = atm.apply_action("FLM")
        self.assertEqual(state, 0)

        processed_volume = atm.transaction(volume=19)
        self.assertEqual(processed_volume, 19)
        self.assertEqual(atm.status["code"], 0)

        processed_volume = atm.transaction(volume=20)
        self.assertEqual(processed_volume, 1)
        self.assertEqual(atm.status["code"], 1100)


    def test_apply_action(self):
        atm = Atm2(ncr001, service_config_3)

        # 1. Восстанавливаем полный ресурс
        processed_volume = atm.transaction()
        self.assertEqual(processed_volume, 140)
        self.assertEqual(atm.status["code"], 1000)

        state, processed_volume, success, down_time, penalty = atm.apply_action("FLM")
        self.assertEqual(state, 0)
        self.assertEqual(atm.devices["CH"].error_codes[1000]["available_resource"], 200)

        # 2. Восстанавливаем частично ресурс
        processed_volume = atm.transaction()
        self.assertEqual(processed_volume, 20)
        self.assertEqual(atm.status["code"], 1100)

        state, processed_volume, success, down_time, penalty = atm.apply_action("FLM")
        self.assertEqual(state, 0)
        self.assertEqual(atm.devices["CH"].error_codes[1100]["available_resource"], 20)

        # 3. Еще раз полностью восстанавливаем ресурс
        processed_volume = atm.transaction()
        self.assertEqual(processed_volume, 20)
        self.assertEqual(atm.status["code"], 1100)

        state, processed_volume, success, down_time, penalty = atm.apply_action("SLM")
        self.assertEqual(state, 0)
        self.assertEqual(atm.devices["CH"].error_codes[1100]["available_resource"], 200)

        # 4. Неудачное действие
        processed_volume = atm.transaction()
        self.assertEqual(processed_volume, 5)
        self.assertEqual(atm.status["code"], 1200)

        state, processed_volume, success, down_time, penalty = atm.apply_action("FLM")
        self.assertEqual(state, 1200)
        self.assertEqual(down_time, 4)
        self.assertEqual(success, False)

        # 5. Действие отсутствует в списке
        state, processed_volume, success, down_time, penalty = atm.apply_action("INK")
        self.assertEqual(state, 1200)
        self.assertEqual(success, False)
        self.assertEqual(down_time, 0)
        self.assertEqual(penalty, 2)

        # 6. Штраф за действие при статусе ОК
        state, processed_volume, success, down_time, penalty = atm.apply_action("SLM")
        self.assertEqual(state, 0)
        self.assertEqual(atm.devices["CH"].error_codes[1200]["available_resource"], 200)

        # 7. Списание транзакций
        state, processed_volume, success, down_time, penalty = atm.apply_action("TRANSACTION", volume=1)
        self.assertEqual(state, 0)
        self.assertEqual(processed_volume, 1)
        self.assertEqual(atm.devices["CH"].error_codes[1200]["available_resource"], 199)







if __name__ == '__main__':
    unittest.main()
