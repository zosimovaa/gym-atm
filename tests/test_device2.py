import unittest
import copy
from gym_atm.envs.device import Device2, AtmDeviceError

ch = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 200,           # Ресурс
            "available_resource": 140,
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        1100: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 160,
            "FLM": [1.0, 0.1],
            "SLM": [1.0, 1.0]
        },
        1200: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 185,
            "FLM": [0.2, 1.0],
            "SLM": [1.0, 1.0]
        },
        1300: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 201,
            "FLM": [0.0, 0.0],
            "SLM": [1.0, 1.0]
        }
    }
}


class Device2TestCase(unittest.TestCase):
    def test_main(self):
        # 0. Создаем объект
        dev = Device2(copy.deepcopy(ch))

        # 1. Проверяем доступный ресурс
        ar = dev.get_available_resource()
        self.assertEqual(ar, 140)

        # 2. Утилизируем ресурс (без ошибки)
        # 2.1 Обучная утилизация
        state, processed_volume = dev.utilize_resource(volume=20)
        self.assertEqual(state, 0)
        self.assertEqual(processed_volume, 20)
        ar = dev.get_available_resource()
        self.assertEqual(ar, 120)

        # 2.2 Ускоряем процесс, чтобы следующий шаг был с ошибкой
        state, processed_volume = dev.utilize_resource(volume=110)
        self.assertEqual(state, 0)
        self.assertEqual(processed_volume, 110)
        ar = dev.get_available_resource()
        self.assertEqual(ar, 10)

        # 3. Списание с ошибкой
        state, processed_volume = dev.utilize_resource(volume=20)
        self.assertEqual(state, 1000)
        self.assertEqual(processed_volume, 10)
        ar = dev.get_available_resource()
        self.assertEqual(ar, 0)

        # 4. Пробуем повторно списать
        state, processed_volume = dev.utilize_resource(volume=20)
        self.assertEqual(state, 1000)
        self.assertEqual(processed_volume, 0)

        # 5. Восстанавливаем ресурс
        success, restored_resource, is_fail_claim = dev.apply_action("FLM")
        self.assertEqual(success, True)
        self.assertEqual(restored_resource, 200)
        self.assertEqual(is_fail_claim, False)

        self.assertEqual(dev.error_codes[1000]["available_resource"], 200)

        state, processed_volume = dev.utilize_resource(volume=20)
        self.assertEqual(state, 1100)
        self.assertEqual(processed_volume, 20)


    def test_get_available_resource(self):
        dev = Device2(copy.deepcopy(ch))
        ar = dev.get_available_resource()
        self.assertEqual(ar, 140)

    def test_utilize_resource(self):
        dev = Device2(copy.deepcopy(ch))

        # 1. Проверка доступного ресурса
        ar = dev.get_available_resource()
        self.assertEqual(ar, 140)

        # 2. Утилизация без приведения к ошибке
        state, processed_volume = dev.utilize_resource(volume=100)
        self.assertEqual(state, 0)
        self.assertEqual(processed_volume, 100)
        ar = dev.get_available_resource()
        self.assertEqual(ar, 40)

        # 3. Утилизация с приведением к ошибке
        state, processed_volume = dev.utilize_resource()
        self.assertEqual(state, 1000)
        self.assertEqual(processed_volume, 40)
        ar = dev.get_available_resource()
        self.assertEqual(ar, 0)



    def test_apply_action(self):
        dev = Device2(copy.deepcopy(ch))
        self.assertEqual(True, True)

        # 1. Экшн на работающем банкомате (ложный выезд)
        success, restored_resource, is_fail_claim = dev.apply_action("FLM")
        self.assertEqual(success, False)
        self.assertEqual(restored_resource, 0)
        self.assertEqual(is_fail_claim, True)

        # 2. Успешное применение действия
        state, processed_volume = dev.utilize_resource()
        success, restored_resource, is_fail_claim = dev.apply_action("FLM")
        self.assertEqual(success, True)
        self.assertEqual(restored_resource, 200)
        self.assertEqual(is_fail_claim, False)

        # 3. Частичное восстановление ресурса
        state, processed_volume = dev.utilize_resource()
        self.assertEqual(1100, state)
        success, restored_resource, is_fail_claim = dev.apply_action("FLM")
        self.assertEqual(success, True)
        self.assertEqual(restored_resource, 20)
        self.assertEqual(is_fail_claim, False)

        state, processed_volume = dev.utilize_resource()
        self.assertEqual(1100, state)
        success, restored_resource, is_fail_claim = dev.apply_action("SLM")
        self.assertEqual(success, True)
        self.assertEqual(restored_resource, 200)
        self.assertEqual(is_fail_claim, False)
        self.assertEqual(0, dev.state)

        # 4. Неуспешное применение действия
        state, processed_volume = dev.utilize_resource()
        self.assertEqual(1200, state)
        dev.apply_action("SLM")

        state, processed_volume = dev.utilize_resource()
        self.assertEqual(1300, state)

        success, restored_resource, is_fail_claim = dev.apply_action("FLM")
        self.assertEqual(success, False)
        self.assertEqual(restored_resource, 0)
        self.assertEqual(is_fail_claim, False)

        # 5. Действие отсутствсвует в списке
        success, restored_resource, is_fail_claim = dev.apply_action("INK")
        self.assertEqual(success, False)
        self.assertEqual(restored_resource, 0)
        self.assertEqual(is_fail_claim, True)
        self.assertEqual(1300, dev.state)

    def test_get_codes(self):
        dev = Device2(copy.deepcopy(ch))
        self.assertEqual(True, True)
        pass

if __name__ == '__main__':
    unittest.main()
