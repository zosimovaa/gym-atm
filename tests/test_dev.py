import unittest
import logging
import numpy as np
from gym_atm.envs.device import Device, AtmDeviceError

logging.basicConfig(level=logging.ERROR, format='%(asctime)s %(name)s [%(levelname)s]: %(message)s')

ch = {
    "name": "Cash Handler",
    "alias": "CH",
    "errors": {
        1000: {
            "err_level": 2,            # 0 - info, 1 - warn, 2 - fatal
            "resource": 200,           # Ресурс
            "available_resource": 140,
            "FLM": [1.0, 1.0],         # Первое число показывает вероятность успеха, второе - восстановление ресурса в случае успеха
            "SLM": [1.0, 1.0]
        },
        1100: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 160,
            "FLM": [1.0, 0.1],
            "SLM": [1.0, 1.0]
        },
        1200: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 180,
            "FLM": [0.2, 1.0],
            "SLM": [1.0, 1.0]
        },
        1300: {
            "err_level": 2,
            "resource": 200,
            "available_resource": 200,
            "FLM": [0.0, 0.0],
            "SLM": [1.0, 1.0]
        }
    }
}

class TestModule(unittest.TestCase):


    def test_main_use_case(self):
        dev = Device(ch)

        # 1. Проверка ресурса
        self.assertEqual(dev.get_available_resource(), 140)

        # 2. Утилизация части ресурса
        self.assertEqual(dev.utilize_resource(volume=10), (0, 10))
        self.assertEqual(dev.get_available_resource(), 130)

        # 3. Падаем в ошибку
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.get_available_resource(), 0)
        self.assertEqual(dev.state, 1000)

        # 4. Чиним ошибку, проверяем остаток
        self.assertEqual(dev.apply_action("FLM"), (0, 200, False))
        self.assertEqual(dev.get_available_resource(), 20)

        # 5. Падаем в следующую ошибку
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.get_available_resource(), 0)
        self.assertEqual(dev.state, 1100)

        # 6. Чиним ошибку, частично восстанавливаем ресурс, проверяем остаток
        self.assertEqual(dev.apply_action("FLM"), (0, 20, False))
        self.assertEqual(dev.get_available_resource(), 20)

        # 7. Падаем в следующую ошибку
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.get_available_resource(), 0)
        self.assertEqual(dev.state, 1100)

        # 8. Чиним ошибку, полностью восстанавливаем ресурс, проверяем остаток
        self.assertEqual(dev.apply_action("SLM"), (0, 200, False))
        self.assertEqual(dev.get_available_resource(), 20)








        pass


    def test_get_available_resource(self):
        dev = Device(ch)

        # 1. Проверка ресурса
        self.assertEqual(dev.get_available_resource(), 160)

        # 2. Утилизация части ресурса и проверка
        self.assertEqual(dev.utilize_resource(volume=10), (0, 10))
        self.assertEqual(dev.get_available_resource(), 150)

        # 3. Утилизация всего ресурса
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.get_available_resource(), 0)

    def test_utilize_resource(self):
        dev = Device(ch)

        # 1. Утилизация части ресурса и проверка
        self.assertEqual(dev.utilize_resource(volume=100), (0, 100))
        self.assertEqual(dev.get_available_resource(), 60)

        # 2. Утилизация всего ресурса
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.get_available_resource(), 0)

        # 3. Попытка утилизации ресурса узла в ошибке
        self.assertEqual(dev.utilize_resource(volume=100), (1000, 0))

        # 4. Попытка утилизации ресурса с отрицательным значением
        self.assertEqual(dev.apply_action("FLM"), (0, 200, False))
        self.assertEqual(dev.get_available_resource(), 20)
        self.assertEqual(dev.utilize_resource(volume=-100), (0, 0))

    def test_apply_action(self):
        dev = Device(ch)
        # 1. Действие при статусе ОК
        self.assertEqual(dev.apply_action("FLM"), (0, 0, True))

        # 2. Успешое устранение ошибки
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.apply_action("FLM"), (0, 200, False))

        # 3. Восстановление части ресурса
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.apply_action("FLM"), (12, 40, False))

        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.apply_action("SLM"), (0, 400, False))

        # 4. Неуспешное устранение ошибки
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.apply_action("FLM"), (1200, 0, False))

        self.assertEqual(dev.apply_action("SLM"), (0, 550, False))

        # 5. Действие не из списка
        self.assertRaises(AtmDeviceError, dev.utilize_resource)
        self.assertEqual(dev.apply_action("ZRM"), (1000, 0, True))

    def test_get_codes(self):
        dev = Device(ch)
        codes = dev.get_codes()
        self.assertEqual(codes[0], 1000)
        self.assertEqual(codes[1], 1100)
        self.assertEqual(codes[2], 1200)
        self.assertEqual(len(codes), 3)


if __name__ == '__main__':
    unittest.main()