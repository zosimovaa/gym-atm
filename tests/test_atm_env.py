import unittest
import gym
import gym_atm

from gym_atm.envs.config.hw_config import ncr001
from gym_atm.envs.config.service_config import service_config_3


class EnvAtmTestCase(unittest.TestCase):

    def test_first(self):
        env = gym.make('atm-env-v0', atm_config=ncr001, service_config=service_config_3)
        observation = env.reset()
        observation, reward, done, info = env.step("TRANSACTION")
        #print(observation)
        #print(reward)




if __name__ == '__main__':
    unittest.main()
